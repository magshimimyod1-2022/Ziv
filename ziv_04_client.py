import socket

IP = socket.gethostname()  # Self
PORT = 7105
RECV = 1024
PASSWORD = 7106
client: socket.socket
CODE = 0
DATA = 1
QUIT = 8


def send(text: str):
    global client
    client.send(text.encode())


def recv():
    global client

    return client.recv(RECV).decode()


def get_password():
    input_user = None

    while input_user != PASSWORD:
        input_user = input("Enter password: ")
        try:
            input_user = int(input_user)
        except TypeError:
            print("Try again...")


def print_albums():
    print("1 Get Albums")
    print("2 Get Albums songs")
    print("3 Get Song Length")
    print("4 Get Song Lyrics")
    print("5 Get Song Album")
    print("6 Search Song by Name")
    print("7 Search Song by Lyrics")
    print("8 Quit")


def get_function_from_user():
    MAX = 9
    MIN = 0

    while True:
        function_choice = input("Enter number: ")

        try:
            function_choice = int(function_choice)

            if MIN < function_choice < MAX:
                return function_choice
        except:
            pass

        print("Wrong choice! try again...")


def get_data():
    data = input("Enter data: ")

    return data


def main():
    global client
    functions_with_data = [2, 3, 4, 5, 6, 7]

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client.connect((IP, PORT))
    except ConnectionRefusedError:
        print("Server is not online")
        exit(1)

    server_message = recv()

    print(f"Server: {server_message.split(':')[DATA]}")

    get_password()

    while True:
        print_albums()

        num = get_function_from_user()

        data = ""

        if num in functions_with_data:
            data = get_data()

        request = f"300:{num}:{data}"

        try:
            send(request)
            server_message = recv()
        except ConnectionResetError:
            print("Connection reset")
            exit(1)

        print("Server: " + server_message.split(":")[DATA])  # Show only DATA part

        if num == QUIT:
            print("Quiting...")
            exit(0)


if __name__ == '__main__':
    main()
