"""
The Hangman game. Really nice and simple...
Created by Ziv Sion. Finished right before the deadline (23:21)
Hope this documentation won't hurt my grade. It just really fun.
I'm wasting my time on it. Why
"""

HANGMAN_PHOTOS = {
    1: "    x-------x\n",
    2: "    x-------x\n" + "    |\n" + "    |\n" + "    |\n" + "    |\n" + "    |\n",
    3: "    x-------x\n" + "    |       |\n" + "    |       0\n" + "    |\n" + "    |\n" + "    |\n",
    4: "    x-------x\n" + "    |       |\n" + "    |       0\n" + "    |       |\n" + "    |\n" + "    |\n",
    5: "    x-------x\n" + "    |       |\n" + "    |       0\n" + "    |      /|\\\n" + "    |\n" + "    |\n",
    6: "    x-------x\n" + "    |       |\n" + "    |       0\n" + "    |      /|\\\n" + "    |      /\n" + "    |\n",
    7: "    x-------x\n" + "    |       |\n" + "    |       0\n" + "    |      /|\\\n" + "    |      / \\\n" + "    |"
}


def print_hangman(num_of_tries):
    print(HANGMAN_PHOTOS[num_of_tries + 1])


def check_win(secret_word, old_letters_guessed):
    """

    :param secret_word: string - secret word the user needs to guess
    :param old_letters_guessed: list of all guessed letters so far
    :return: boolean value if user won
    """

    for letter in secret_word:
        if letter not in old_letters_guessed:
            return False

    return True


def show_hidden_word(secret_word, old_letters_guessed):
    """

    :param secret_word: the secret word the user needs to guess
    :param old_letters_guessed: list of all used letters so far
    :return: list of bottom dashes, and letters
    """

    new_list = ""

    for letter in secret_word:
        if letter in old_letters_guessed:
            new_list += letter
        else:
            new_list += "_"

    return " ".join(new_list)


def is_valid_input(letter_guessed):
    """
    Function checks length of input and if it's alpha
    :param letter_guessed: input letter guessed by the user to check validity
    :return: True if the input is valid and False if isn't
    """

    if (len(letter_guessed) > 1) or (not str(letter_guessed).isalpha()):
        return False

    return True


def check_valid_input(letter_guessed: str, old_letters_guessed: list):
    """

    :param letter_guessed: input from the user
    :param old_letters_guessed: get guessed letters
    :return: boolean value if input is valid and user did not guess that letter before
    """
    return is_valid_input(letter_guessed) and letter_guessed.lower() not in old_letters_guessed


def try_update_letter_guessed(letter_guessed: str, old_letters_guessed: list):
    """
    Function checks if letter is valid - not guessed and alpha and not used before.
    If True - adds the letter to the old_letters_guessed
    If False - prints X and prints the used letters and returns False
    :param letter_guessed: input from the user
    :param old_letters_guessed: list of guessed letters
    :return: boolean value - true if insertion of value occurred successfully, or false if letter is invalid or the letter is already in the old letters list
    """

    # False:
    if not check_valid_input(letter_guessed, old_letters_guessed):
        print("X")
        # Print guessed letters
        old_letters_guessed_str = " -> ".join(sorted(old_letters_guessed, key=str.lower))
        old_letters_guessed_str = old_letters_guessed_str[::-1].replace(" <- ", "", 1)[::-1]
        print(old_letters_guessed_str)
        return False

    else:
        old_letters_guessed += letter_guessed
        return True


def choose_word(file_path, index):
    """
    Function gets path of file that contains list of words, and index of certain word.
    Returns tuple with two elements: the first is the number of the distinct words in the file.
    The second is the word in the index that got as parameter as the secret word (starts from 1)
    If index is bigger than number of words in the file, the function will keep searching from start.

    File content for example:
    "hangman song most broadly is a song hangman work music work broadly is typically"

    :param file_path: path to the file
    :param index: index of certain word
    :return: tuple of two elements
    """

    file = open(file_path, "r")
    words = file.read().split(" ")
    file.close()

    total_num_of_words = len(words)

    # Remove duplicates
    duplicates = []
    words_new = []
    for word in words:
        if word not in duplicates:
            words_new += [word]
            duplicates += [word]

    num_of_words = len(words_new)

    while index > total_num_of_words:
        index = index - total_num_of_words

    return num_of_words, words[index - 1]  # Index starts from 1


def is_bad_guess(secret_word: str, letter_guessed: str):
    return letter_guessed not in secret_word


# Variables
MAX_TRIES = 6
user_input = ""
HANGMAN_ASCII_ART = "  _    _                                         \n" + " | |  | |                                        \n" + ' | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  \n' + " |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ \n" + ' | |  | | (_| | | | | (_| | | | | | | (_| | | | |\n' + ' |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|\n' + '                      __/ |                      \n' + '                     |___/'


def main():
    old_letters_guessed = []
    num_of_tries = 0

    # Print the Welcome Screen
    print(HANGMAN_ASCII_ART)

    # Get file path and index to the secret word
    file_path = input("Enter file path: ")
    index = int(input("Enter index: "))

    # Set the secret word by the user
    secret_word = choose_word(file_path, index)[1]

    print("\nLet's start!\n")

    # Print hangman position
    print_hangman(num_of_tries)

    # Print hint _ _ _ _
    print(show_hidden_word(secret_word, old_letters_guessed))

    while not check_win(secret_word, old_letters_guessed):
        # Input
        input_letter = input("\nGuess a letter: ").lower()

        # Ask for another guess until guess is valid
        while not try_update_letter_guessed(input_letter, old_letters_guessed):
            input_letter = input("\nGuess a letter: ").lower()

        # If input is valid but guess is wrong
        if is_bad_guess(secret_word, input_letter):

            # Valid but wrong guess
            num_of_tries += 1

            # Sad face
            print(":(")

            # Print hangman position
            print_hangman(num_of_tries)

            # Lose
            if num_of_tries == MAX_TRIES:
                # Show hint as _ _ _ _
                print(show_hidden_word(secret_word, old_letters_guessed))

                print("LOSE")
                exit()

        # Show hint as _ _ _ _
        print(show_hidden_word(secret_word, old_letters_guessed))

    print("WIN")


if __name__ == '__main__':
    main()
