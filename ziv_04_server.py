import socket

IP = ""  # Self
PORT = 7105
RECV = 1024  # Bytes to receive every call
content = ""
client: socket.socket
functions_with_data = [2, 3, 4, 5, 6, 7]
CODE = 0
FUNC = 1
DATA = 2
NUM_TO_LISTEN = 5
# Codes:
SUCCESS = 200
ERROR_NO_DATA_FOUND = "100:No Data Found"
ERROR_DATA_INVALID = "101:Data Invalid"
WELCOME = "Welcome, Pink Floyd fan!"

def send(text):
    text = str(text)

    if text == "":  # No data found to send to the client
        client.send(ERROR_NO_DATA_FOUND.encode())
    else:  # Send success code and data
        client.send((str(SUCCESS) + ":" + text).encode())


def recv():
    return client.recv(RECV).decode()


def album_list():
    """

    :return: list of all albums in the DB
    """

    return "Album list"


def songs_list(album: str):
    """

    :param album: album name to get the songs from
    :return: list of songs found in specific album
    """
    return "Songs in this album: "


def song_length(song_name: str):
    """

    :param song_name: a song name to look for
    :return: length of the song
    """
    return "The song length is"


def lyrics_of(song_name: str):
    """

    :param song_name: a song name to get it's lyrics
    :return: lyrics of the song
    """
    return "The lyrics are:"


def what_album(song_name: str):
    """

    :param song_name: song name to look for
    :return: the album name that contains the song
    """
    return "The album that contains this song is"


def songs_by(word: str):
    """

    :param word: string word to look for
    :return: list of song names that contains that word
    """

    word = word.lower()

    return "The songs that contains this word in their name are:"


def songs_contains(word: str):
    """

    :param word: word to search in every song lyrics
    :return: all songs that contains that word in their lyrics
    """
    return "Songs that contains that word in their lyrics are:"


functions = {
    1: album_list,
    2: songs_list,
    3: song_length,
    4: lyrics_of,
    5: what_album,
    6: songs_by,
    7: songs_contains,
}


def handle():
    """
    Function that gets the data from the client and activates the function by it's data
    :return: None
    """
    command = recv()  # Should be enough
    print("Client: " + command)

    request_parts = command.split(":")

    func = request_parts[FUNC]
    data = request_parts[DATA]

    if func == '8':  # Client wants to quit
        print("Client wants to disconnect")
        return "QUIT"  # Raise exception to break the while loop in main function

    """
    
    Server:
    CODE|DATA
    Client:
    CODE|FUNCTION|DATA
    
    Example for command - Client:
    300:2:wall of cool thing
        
    Index:
    0 - ['300']
    1 - [2]
    2 - [The wall]
        
    Error codes - Server
    100:Data given is invalid
    100:No data found
    """

    # Activate function by integer
    if int(func) in functions_with_data:
        send(functions[int(func)](data))
    else:
        send(functions[int(func)]())


def main():
    global content

    # Read content of the data base
    content = read_data()

    # Create the socket connection
    server = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    server.bind((IP, PORT))

    print(f"Running on port {PORT} | with ip: {socket.gethostname()}...")

    server.listen(NUM_TO_LISTEN)  # Listen up to 5 connection (for part B)

    # Start listening
    while True:
        global client

        client, _ = server.accept()  # Accept new connection

        send(WELCOME)  # Send hello when connection is created

        while True:
            try:
                message = handle()

                # Client asked to quit
                if message == "QUIT":
                    send("Good bye. Keep listening to Pink Floyd!")
                    break
            except ConnectionResetError:  # Connection stopped, keep listening
                break
            except Exception as e:
                print("Error: ", e)
                break


def read_data():
    with open(r"C:\Users\test0\OneDrive\שולחן העבודה\מגשימים קורס רשתות\סמסטר ב\week4\Pink Floyd\Pink_Floyd_DB.txt",
              "r") as file:
        content = file.read()
        file.close()

    return content


if __name__ == '__main__':
    main()
