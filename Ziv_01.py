import requests, time
from collections import Counter

def extract_password_from_site():
    URL = "http://webisfun.cyber.org.il/nahman/files"
    num = 11
    password = ""

    while num <= 34:
        file_name = "file" + str(num) + ".nfo"
        response = requests.get(URL + "/" + file_name).text

        password += response[99]
        num += 1

        time.sleep(0.1)

    return password
def find_most_common_words(path, num_of_words):
    c = None
    # If user entered url
    if "http" in path:
        response = requests.get(path).text
        c = Counter(response.split(" "))
    # If user entered file path
    else:
        file = open(path, "r")
        c = Counter(file.read().split(" "))
        file.close()

    common = c.most_common(num_of_words)

    password = ""

    for word in common:
        password += word[0] + " "

    return password[:-1]

def main():
    option = input("Choose 1 or 2: ")

    if "1" in option:
        print("Please wait...")
        print(extract_password_from_site())
    else:
        url = "http://webisfun.cyber.org.il/nahman/final_phase/words.txt"
        num_of_words = 6
        print(find_most_common_words(url, num_of_words))

if __name__ == '__main__':
    main()
